<?php
// JonatasCraveiro\importacao-xml-esus\src\FormularioContatoServiceProvider.php
namespace JonatasCraveiro\ImportacaoXmlEsus;

use Illuminate\Support\ServiceProvider;

class ImportacaoXmlEsusServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerRoutes();
        $this->registerViews();
        $this->registerMigrations();
        

    }
    public function register()
    {
        
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views', 'importacao-xml-esus');
    }

    protected function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    }

    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }
}
