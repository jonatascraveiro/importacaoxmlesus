<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Repositories;


use Illuminate\Support\Facades\Storage;
use JonatasCraveiro\ImportacaoXmlEsus\Models\ProfissionalXmlEsus;

class ProfissionalXmlEsusRepository
{


  public static function data()
  {
    ProfissionalXmlEsus::truncate();
    $xml = Storage::get('xml/esus.xml');

    $xml   = simplexml_load_string($xml);
    $json  = json_encode($xml);
    $DADOS = json_decode($json, true);

    $erro = 0;

    foreach ($DADOS['IDENTIFICACAO']['PROFISSIONAIS']['DADOS_PROFISSIONAIS'] as $lotacao) {

      $contador = 0;

      try {
        $contador = count($lotacao['LOTACOES']['DADOS_LOTACOES']);
      } catch (\Throwable $th) {
        $contador = 0;
      }

      if (0 === $contador) {
        $identificacao = ProfissionalXmlEsusRepository::identificacaoProfissional($lotacao, $DADOS);
        $lotacao       = ProfissionalXmlEsusRepository::profissionalSemLotacao($lotacao);
        $values        = array_merge($identificacao, $lotacao);
        ProfissionalXmlEsus::create($values);
      }

      if (1 === $contador) {
        $identificacao = ProfissionalXmlEsusRepository::identificacaoProfissional($lotacao, $DADOS);
        $lotacao       = [
          'cnes'      => $lotacao['LOTACOES']['DADOS_LOTACOES']['@attributes']['CNES'],
          'co_ine'    => $lotacao['LOTACOES']['DADOS_LOTACOES']['@attributes']['CO_INE'],
          'co_cbo'    => $lotacao['LOTACOES']['DADOS_LOTACOES']['@attributes']['CO_CBO'],
          'microarea' => ProfissionalXmlEsusRepository::verificaSeExistePropriedadeMicroArea($lotacao, false),
        ];
        $values = array_merge($identificacao, $lotacao);

        ProfissionalXmlEsus::create($values);
      }

      if ($contador > 1) {

        $identificacao = ProfissionalXmlEsusRepository::identificacaoProfissional($lotacao, $DADOS);

        for ($i = 0; $i < count($lotacao['LOTACOES']['DADOS_LOTACOES']); $i++) {

          $lotacao_profissional = [
            'cnes'      => $lotacao['LOTACOES']['DADOS_LOTACOES'][$i]['@attributes']['CNES'],
            'co_ine'    => $lotacao['LOTACOES']['DADOS_LOTACOES'][$i]['@attributes']['CO_INE'],
            'co_cbo'    => $lotacao['LOTACOES']['DADOS_LOTACOES'][$i]['@attributes']['CO_CBO'],
            'microarea' => ProfissionalXmlEsusRepository::verificaSeExistePropriedadeMicroArea($lotacao, $i),
          ];
          $values = array_merge($identificacao, $lotacao_profissional);
          ProfissionalXmlEsus::create($values);
        }
      }
    }
  }

  public static function identificacaoProfissional($lotacao, $DADOS)
  {
    return [
      'data' => $DADOS['IDENTIFICACAO']['@attributes']['DATA'],
      'co_ibge_mun' => $DADOS['IDENTIFICACAO']['@attributes']['CO_IBGE_MUN'],
      'nm_prof'     => $lotacao['@attributes']['NM_PROF'],
      'cpf_prof'    => $lotacao['@attributes']['CPF_PROF'],
      'co_cns'      => $lotacao['@attributes']['CO_CNS'],
      'dt_nasc'     => $lotacao['@attributes']['DT_NASC'],
      'sexo'        => $lotacao['@attributes']['SEXO'],
      'conselho_id' => $lotacao['@attributes']['CONSELHO_ID'],
      'sg_uf_emis'  => $lotacao['@attributes']['SG_UF_EMIS'],
      'nu_registro' => $lotacao['@attributes']['NU_REGISTRO'],
      'cnes'        => '',
      'co_ine'      => '',
      'co_cbo'      => '',
      'microarea'   => '',

    ];
  }

  public static function profissionalSemLotacao()
  {
    return [
      'cnes'      => '',
      'co_ine'    => '',
      'co_cbo'    => '',
      'microarea' => '',

    ];
  }

  public static function verificaSeExistePropriedadeMicroArea($lotacao, $i)
  {
    try {
      if ($i == false) {
        return $lotacao['LOTACOES']['DADOS_LOTACOES'][$i]['@attributes']['MICROAREA'];
      } else {
        $lotacao['LOTACOES']['DADOS_LOTACOES']['@attributes']['MICROAREA'];
      }
    } catch (\Throwable $th) {
      return '';
    }
  }
}
