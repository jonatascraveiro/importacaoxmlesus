<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Repositories;

use Illuminate\Support\Facades\Storage;
use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;

class UnidadeXmlEsusRepository
{

  public static function data()
  {
    UnidadeXmlEsus::truncate();

    $xml = Storage::get('xml/esus.xml');

    $xml   = simplexml_load_string($xml);
    $json  = json_encode($xml);
    $DADOS = json_decode($json, true);
// dd($DADOS['IDENTIFICACAO']['@attributes']['DATA']);
    foreach ($DADOS['IDENTIFICACAO']['ESTABELECIMENTOS']['DADOS_GERAIS_ESTABELECIMENTOS'] as $unidade) {

      $complexidade = UnidadeXmlEsusRepository::complexidade($unidade);

      $values = [
      
        'data'=>$DADOS['IDENTIFICACAO']['@attributes']['DATA'],
        'nm_fanta'     => $unidade['@attributes']['NM_FANTA'],
        'cnpj'         => $unidade['@attributes']['CNPJ'],
        'cnes'         => $unidade['@attributes']['CNES'],
        'tp_unid_id'   => $unidade['@attributes']['TP_UNID_ID'],
        'ds_tp_unid'   => UnidadeXmlEsusRepository::verificaSeExistePropriedadeTpUnidade($unidade),
        'co_esf_adm'   => UnidadeXmlEsusRepository::verificaSeExistePropriedadeCoEsfAdm($unidade),
        'telefone1'    => $unidade['@attributes']['TELEFONE1'],
        'telefone2'    => $unidade['@attributes']['TELEFONE2'],
        'fax'          => $unidade['@attributes']['FAX'],
        'e_mail'       => $unidade['@attributes']['E_MAIL'],
        'co_cep'       => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['CO_CEP'],
        'sg_uf'        => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['SG_UF'],
        'co_ibge_mun'  => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['CO_IBGE_MUN'],
        'bairro'       => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['BAIRRO'],
        'logradouro'   => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['LOGRADOURO'],
        'numero'       => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['NUMERO'],
        'complement'   => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['COMPLEMENT'],
        'ponto_ref'    => $unidade['ENDERECO']['DADOS_ENDERECO']['@attributes']['PONTO_REF'],
        'complexidade' => $complexidade,

      ];

      UnidadeXmlEsus::create($values);
    }
  }

  public static function complexidade($unidade)
  {

    if (count($unidade['COMPLEXIDADE']['DADOS_COMPLEXIDADE']) > 0) {

      if (count($unidade['COMPLEXIDADE']['DADOS_COMPLEXIDADE']) == 1) {
        return $unidade['COMPLEXIDADE']['DADOS_COMPLEXIDADE']['@attributes']['SG_COMPLEXIDADE'];
      } else {
        $complexidade = [];
        for ($i = 0; $i < count($unidade['COMPLEXIDADE']['DADOS_COMPLEXIDADE']); $i++) {
          try {
            array_push($complexidade, $unidade['COMPLEXIDADE']['DADOS_COMPLEXIDADE'][$i]['@attributes']['SG_COMPLEXIDADE']);
          } catch (\Throwable $th) {
            dd($unidade);
          }
        }

        return collect($complexidade)->join(' | ');
      }
    } else {
      return '';
    }
  }

 public static function verificaSeExistePropriedadeTpUnidade($unidade)
  {
    try {
      return $unidade['@attributes']['DS_TP_UNID'];
    } catch (\Throwable $th) {
      return '';
    }
  }

  public static function verificaSeExistePropriedadeCoEsfAdm($unidade)
  {
    try {
      return $unidade['@attributes']['CO_ESF_ADM'];
    } catch (\Throwable $th) {
      return '';
    }
  }
}
