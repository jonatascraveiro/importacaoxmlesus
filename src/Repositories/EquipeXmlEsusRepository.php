<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Repositories;

use Illuminate\Support\Facades\Storage;
use JonatasCraveiro\ImportacaoXmlEsus\Models\EquipeXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;

class EquipeXmlEsusRepository
{

    public static function data()
    {

        EquipeXmlEsus::truncate();
        $xml =   Storage::get('xml/esus.xml');

        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $DADOS = json_decode($json, TRUE);

        $erro  = 0;

        foreach ($DADOS['IDENTIFICACAO']['ESTABELECIMENTOS']['DADOS_GERAIS_ESTABELECIMENTOS'] as $equipe) {
            try {
                $contador = count($equipe['EQUIPES']['DADOS_EQUIPES']);
            } catch (\Throwable $th) {
                $contador = 0;
                $erro++;
            }

            if (1 === $contador) {

                $values = [
                    'data' => $DADOS['IDENTIFICACAO']['@attributes']['DATA'],
                    'co_ibge_mun' => $DADOS['IDENTIFICACAO']['@attributes']['CO_IBGE_MUN'],
                    'cnes_unidade'   => $equipe['@attributes']['CNES'],
                    'id_tp_equipe'   => EquipeXmlEsusRepository::verificaSeExistePropriedadeTpEquipe($equipe, false),
                    'tp_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['TP_EQUIPE'],
                    'sg_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['SG_EQUIPE'],
                    'ds_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['DS_EQUIPE'],
                    'co_ine'         => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['CO_INE'],
                    'co_area'        => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['CO_AREA'],
                    'ds_area'        => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['DS_AREA'],
                    'nm_referencia'  => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['NM_REFERENCIA'],
                    'dt_desativacao' => $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['DT_DESATIVACAO'],
                    'ine_nasf'       => '',

                ];
                EquipeXmlEsus::create($values);
            }

            if ($contador > 1) {
                for ($i = 0; $i < count($equipe['EQUIPES']['DADOS_EQUIPES']); $i++) {
                    $values = [
                        'data' => $DADOS['IDENTIFICACAO']['@attributes']['DATA'],
                        'co_ibge_mun' => $DADOS['IDENTIFICACAO']['@attributes']['CO_IBGE_MUN'],
                        'cnes_unidade'   => $equipe['@attributes']['CNES'],
                        'id_tp_equipe'   => EquipeXmlEsusRepository::verificaSeExistePropriedadeTpEquipe($equipe, $i),
                        'tp_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['TP_EQUIPE'],
                        'sg_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['SG_EQUIPE'],
                        'ds_equipe'      => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['DS_EQUIPE'],
                        'co_ine'         => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['CO_INE'],
                        'co_area'        => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['CO_AREA'],
                        'ds_area'        => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['DS_AREA'],
                        'nm_referencia'  => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['NM_REFERENCIA'],
                        'dt_desativacao' => $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['DT_DESATIVACAO'],
                        'ine_nasf'       => '',

                    ];

                    EquipeXmlEsus::create($values);
                }
            }
        }
    }

    public static function verificaSeExistePropriedadeTpEquipe($equipe, $i)
    {
        try {
            if ($i == false) {
                return $equipe['EQUIPES']['DADOS_EQUIPES']['@attributes']['ID_TP_EQUIPE'];
            } else {
                return $equipe['EQUIPES']['DADOS_EQUIPES'][$i]['@attributes']['ID_TP_EQUIPE'];
            }
        } catch (\Throwable $th) {
            return '';
        }
    }
}
