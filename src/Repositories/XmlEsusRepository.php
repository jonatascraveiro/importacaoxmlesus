<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Repositories;

use Illuminate\Support\Facades\Storage;
use JonatasCraveiro\ImportacaoXmlEsus\Models\EquipeXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;

class XmlEsusRepository
{

    public static function salvarXml($request): void
    {
        $request->file('xml')->storeAs(
            'xml',
            'esus.xml'
        );
    }

    public static function apagarXml(): void
    {
        Storage::delete('xml/esus.xml');
    }
}
