<!-- MyVendor\formulario-contato\src\resources\views\contato.blade.php -->
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <title>Importacao xml</title>
</head>

<body>
    <div class="container-fluid mt-6">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12 alert alert-secondary">
            <h3>Importacao xml Esus</h3>
        </div>
        <form action="{{ route('importacao.xml.esus.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-row ">
                <div class="form-group col-3 offset-1 ">
                    <label for="inputXml">Carregue o xml descompactado do arquivo zip</label>
                    <input type="file" class="form-control-file col" name="xml" id="inputXml">
                </div>
                <div class="form-group col  align-self-center ">
                    <button type="submit" class="btn btn-primary ">Enviar</button>

                </div>

            </div>

        </form>

        @if ($xml)

            <div class="row m-4">

                <div class="col-12 alert alert-secondary row">
                    <h3 class="col-6">Processamento de arquivo</h3><a href="{{ route('importacao.xml.esus.delete') }}" ><button class="btn btn-danger">Remover arquivos</button></a>
                </div>


                <form class="col-4 align-self-center " action="{{ route('importacao.xml.esus.profissional') }}"
                    method="POST">
                    @csrf

                    <div>
                        <label>Carregar lotações dos profissionais</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Processar</button>
                </form>

                <form class="col-4 align-self-center" action="{{ route('importacao.xml.esus.unidade') }}"
                    method="POST">
                    @csrf

                    <div>
                        <label>Carregar Unidades de Saúde</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Processar</button>
                </form>

                <form class="col-4 align-self-center" action="{{ route('importacao.xml.esus.equipe') }}"
                    method="POST">
                    @csrf

                    <div>
                        <label>Carregar Equipes de Saúde</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Processar</button>
                </form>
            </div>
            <div class="row m-4">
                <div class="col-12 alert alert-secondary">
                    <h3>Prévia dos Dados Importados</h3>
                </div>
                <div class="col-4">

                    <div class="alert alert-primary row">
                        <a class="col" href="{{ route('visualiza.xml.esus.profissional') }}">
                            <h4>Profissionais (ver dados)</h4>
                        </a>
                        @if (count($profissionais) > 0)
                            <a class="col" href="{{ route('export.xml.esus.profissional') }}">
                                <h4>Baixar dados</h4>
                            </a>
                        @endif
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Cnes</th>
                                <th scope="col">CBO</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($profissionais as $profissional)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $profissional->nm_prof }}</td>
                                    <td>{{ $profissional->co_cns }}</td>
                                    <td>{{ $profissional->co_cbo }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>

                <div class="col-4">

                    <div class="alert alert-primary row">
                        <a class="col" href="{{ route('visualiza.xml.esus.unidade') }}">
                            <h4>Unidades  (ver dados)</h4>
                        </a>
                        @if (count($unidades) > 0)
                            <a class="col" href="{{ route('export.xml.esus.unidade') }}">
                                <h4>Baixar dados</h4>
                            </a>
                        @endif
                    </div>




                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Cnes</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($unidades as $unidades)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $unidades->nm_fanta }}</td>
                                    <td>{{ $unidades->cnes }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-4">
                    <div class="alert alert-primary row">
                        <a class="col" href="{{ route('visualiza.xml.esus.equipe') }}">
                            <h4>Equipes (ver dados)</h4>
                        </a>
                        @if (count($equipes) > 0)
                            <a class="col" href="{{ route('export.xml.esus.equipe') }}">
                                <h4>Baixar dados</h4>
                            </a>
                        @endif
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">TIPO</th>
                                <th scope="col">CNES</th>
                                <th scope="col">INE</th>
                                <th scope="col">Nome</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($equipes as $equipe)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $equipe->tp_equipe }}</td>
                                    <td>{{ $equipe->cnes_unidade }}</td>
                                    <td>{{ $equipe->co_ine }}</td>
                                    <td>{{ $equipe->nm_referencia }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    </div>

    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>
