<!-- MyVendor\formulario-contato\src\resources\views\contato.blade.php -->
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


  <title>Importacao xml</title>
</head>

<body>
  <div class="container mt-6">
    @if(session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="row m-4">
      
      <div class="col-12">

        <div class="alert alert-primary row">
          <h4 class="col-11">Profissional</h4> 
          <a class="col-1 btn btn-secondary" href="{{route('importacao.xml.esus.index')}}"> Voltar</a>
        </div>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">CNES</th>
              <th scope="col">INE</th>
              <th scope="col">Nome</th>
              <th scope="col">CNS</th>
              <th scope="col">CBO</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($profissionais as $profissional)
            <tr>
              <th scope="row">{{$profissional->id}}</th>
              <td>{{$profissional->cnes}}</td>
              <td>{{$profissional->co_ine}}</td>
              <td>{{$profissional->nm_prof}}</td>
              <td>{{$profissional->co_cns}}</td>
              <td>{{$profissional->co_cbo}}</td>
            </tr>
            @endforeach

          </tbody>
        </table>
        {{$profissionais->links()}}
      </div>
    </div>
  </div>
  </div>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
  </script>
</body>

</html>