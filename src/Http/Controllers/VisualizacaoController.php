<?php
// MyVendor\formulario-contato\src\Http\Controllers\FormularioContatoController.php
namespace JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers;

use App\Http\Controllers\Controller;
use JonatasCraveiro\ImportacaoXmlEsus\Models\EquipeXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\ProfissionalXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;

class VisualizacaoController extends Controller
{

 public function profissional()
 {
  $profissionais = ProfissionalXmlEsus::paginate(10);

  return view('importacao-xml-esus::profissional', ['profissionais' => $profissionais]);
 }

 public function unidade()
 {

  $unidades = UnidadeXmlEsus::paginate(10);

  return view('importacao-xml-esus::unidade', ['unidades' => $unidades]);
 }

 public function equipe()
 {

  $equipes = EquipeXmlEsus::paginate(10);

  return view('importacao-xml-esus::equipe', ['equipes' => $equipes]);
 }

}
