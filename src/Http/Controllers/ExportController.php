<?php
// MyVendor\formulario-contato\src\Http\Controllers\FormularioContatoController.php
namespace JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers;

use App\Http\Controllers\Controller;
use JonatasCraveiro\ImportacaoXmlEsus\Exports\EquipeXmlEsusExport;
use JonatasCraveiro\ImportacaoXmlEsus\Exports\UnidadeXmlEsusExport;
use JonatasCraveiro\ImportacaoXmlEsus\Exports\ProfisionalXmlEsusExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{

    public function profissional()
    {

        return Excel::download(new ProfisionalXmlEsusExport, 'profissional_xml_esus.xlsx');
    }

    public function unidade()
    {

        return Excel::download(new UnidadeXmlEsusExport, 'unidade_xml_esus.xlsx');
    }

    public function equipe()
    {

        return Excel::download(new EquipeXmlEsusExport, 'equipe_xml_esus.xlsx');
    }
}
