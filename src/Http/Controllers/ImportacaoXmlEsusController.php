<?php
// MyVendor\formulario-contato\src\Http\Controllers\FormularioContatoController.php
namespace JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JonatasCraveiro\ImportacaoXmlEsus\Models\EquipeXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\ProfissionalXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;
use JonatasCraveiro\ImportacaoXmlEsus\Repositories\EquipeXmlEsusRepository;
use JonatasCraveiro\ImportacaoXmlEsus\Repositories\ProfissionalXmlEsusRepository;
use JonatasCraveiro\ImportacaoXmlEsus\Repositories\UnidadeXmlEsusRepository;
use JonatasCraveiro\ImportacaoXmlEsus\Repositories\XmlEsusRepository;

class ImportacaoXmlEsusController extends Controller
{
    public function index()
    {
        $xml = Storage::exists('xml/esus.xml');

        $unidades      = UnidadeXmlEsus::limit(10)->get();
        $equipes       = EquipeXmlEsus::limit(10)->get();
        $profissionais = ProfissionalXmlEsus::limit(10)->get();
        return view('importacao-xml-esus::importacao', ['xml' => $xml, 'unidades' => $unidades, 'equipes' => $equipes, 'profissionais' => $profissionais]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'xml' => ['required', 'mimes:xml'],
        ]);
        XmlEsusRepository::salvarXml($request);
        return redirect()->back()->with('status', 'Arquivo xml carregado com sucesso');
    }

    public function carregarProfissional()
    {

        ProfissionalXmlEsusRepository::data();
        return redirect()->back()->with('status', 'Dados dos profissionais foram carregados com suceso');
    }
    public function carregarUnidades()
    {
        UnidadeXmlEsusRepository::data();
        return redirect()->back()->with('status', 'Dados das unidades foram carregados com suceso');
    }
    public function carregarEquipes()
    {
        EquipeXmlEsusRepository::data();
        return redirect()->back()->with('status', 'Dados das equipes foram carregados com suceso');
    }

    public function delete()
    {
        XmlEsusRepository::apagarXml();
        UnidadeXmlEsus::truncate();
        EquipeXmlEsus::truncate();
        ProfissionalXmlEsus::truncate();

        return redirect()->back()->with('status', 'Dados foram deletados com sucesso');
    }
}
