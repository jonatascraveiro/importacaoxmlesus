<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfissionaisXmlEsusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profissional_xml_esus', function (Blueprint $table) {
            $table->id();
            $table->string('nm_prof')->nullable();
            $table->string('cpf_prof')->nullable();
            $table->string('co_cns')->nullable();
            $table->string('dt_nasc')->nullable();
            $table->string('sexo')->nullable();
            $table->string('conselho_id')->nullable();
            $table->string('sg_uf_emis')->nullable();
            $table->string('nu_registro')->nullable();
            $table->string('cnes')->nullable();
            $table->string('co_ine')->nullable();
            $table->string('co_cbo')->nullable();
            $table->string('microarea')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profissional_xml_esus');
    }
}
