<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnEquipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipe_xml_esus', function (Blueprint $table) {
            $table->string('co_ibge_mun')->nullable();
            $table->date('data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipe_xml_esus', function (Blueprint $table) {
            $table->dropColumn(['co_ibge_mun','data']);
        });
    }
}
