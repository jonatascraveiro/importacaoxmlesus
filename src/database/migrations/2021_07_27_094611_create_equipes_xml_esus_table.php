<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipesXmlEsusTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('equipe_xml_esus', function (Blueprint $table) {
   $table->id();
   $table->string('id_tp_equipe')->nullable();
   $table->string('tp_equipe')->nullable();
   $table->string('sg_equipe')->nullable();
   $table->string('ds_equipe')->nullable();
   $table->string('co_ine')->nullable();
   $table->string('co_area')->nullable();
   $table->string('ds_area')->nullable();
   $table->string('nm_referencia')->nullable();
   $table->string('dt_desativacao')->nullable();
   $table->string('cnes_unidade')->nullable();
   $table->string('ine_nasf')->nullable();
   
   $table->timestamps();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::dropIfExists('equipe_xml_esus');
 }
}
