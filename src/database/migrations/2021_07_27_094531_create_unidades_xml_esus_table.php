<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadesXmlEsusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidade_xml_esus', function (Blueprint $table) {
            $table->id();
            $table->string('nm_fanta')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('cnes')->nullable();
            $table->string('tp_unid_id')->nullable();
            $table->string('co_esf_adm')->nullable();
            $table->string('telefone1')->nullable();
            $table->string('telefone2')->nullable();
            $table->string('fax')->nullable();
            $table->string('e_mail')->nullable();
            $table->string('co_cep')->nullable();
            $table->string('sg_uf')->nullable();
            $table->string('co_ibge_mun')->nullable();
            $table->string('bairro')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complement')->nullable();
            $table->string('ponto_ref')->nullable();
            $table->string('complexidade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidade_xml_esus');
    }
}
