<?php
// MyVendor\formulario-contato\src\routes\web.php

use Illuminate\Support\Facades\Route;
use JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers\ExportController;
use JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers\ImportacaoXmlEsusController;
use JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers\VisualizacaoController;

Route::get('contato', function(){
    return view('importacao-xml-esus::importacao');
});

Route::group(['namespace' => 'JonatasCraveiro\ImportacaoXmlEsus\Http\Controllers', 'middleware' => ['web']], function(){
    Route::get('importacao-xml-esus', [ImportacaoXmlEsusController::class,'index'])->name('importacao.xml.esus.index');
    Route::post('importacao-xml-esus', [ImportacaoXmlEsusController::class,'store'])->name('importacao.xml.esus.store');
    Route::post('importacao-xml-esus-profissional', [ImportacaoXmlEsusController::class,'carregarProfissional'])->name('importacao.xml.esus.profissional');
    Route::post('importacao-xml-esus-unidade', [ImportacaoXmlEsusController::class,'carregarUnidades'])->name('importacao.xml.esus.unidade');
    Route::post('importacao-xml-esus-equipe', [ImportacaoXmlEsusController::class,'carregarEquipes'])->name('importacao.xml.esus.equipe');
    Route::get('importacao-xml-esus-delete', [ImportacaoXmlEsusController::class,'delete'])->name('importacao.xml.esus.delete');


    Route::get('importacao-xml-esus/equipe', [VisualizacaoController::class,'equipe'])->name('visualiza.xml.esus.equipe');
    Route::get('importacao-xml-esus/unidade', [VisualizacaoController::class,'unidade'])->name('visualiza.xml.esus.unidade');
    Route::get('importacao-xml-esus/profissional', [VisualizacaoController::class,'profissional'])->name('visualiza.xml.esus.profissional');

    Route::get('importacao-xml-esus/export/profissional', [ExportController::class,'profissional'])->name('export.xml.esus.profissional');
    Route::get('importacao-xml-esus/export/unidade', [ExportController::class,'unidade'])->name('export.xml.esus.unidade');
    Route::get('importacao-xml-esus/export/equipe', [ExportController::class,'equipe'])->name('export.xml.esus.equipe');

});