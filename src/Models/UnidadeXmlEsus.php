<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnidadeXmlEsus extends Model {
 protected $table    = 'unidade_xml_esus';
 protected $fillable = [

  'nm_fanta',
  'cnpj',
  'cnes',
  'tp_unid_id',
  'co_esf_adm',
  'telefone1',
  'telefone2',
  'fax',
  'e_mail',
  'co_cep',
  'sg_uf',
  'co_ibge_mun',
  'bairro',
  'logradouro',
  'numero',
  'complement',
  'ponto_ref',
  'complexidade',
  'data',
  
 ];

//  php artisan make:migration create_profissionais_xml_esus_table --path=app/packages/JonatasCraveiro/importacao-xml-esus/src/database/migrations --create=profissionais_xml_esus


}
