<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProfissionalXmlEsus extends Model {
 protected $table    = 'profissional_xml_esus';
 protected $fillable = [

  'nm_prof',
  'cpf_prof',
  'co_cns',
  'dt_nasc',
  'sexo',
  'conselho_id',
  'sg_uf_emis',
  'nu_registro',
  'cnes',
  'co_ine',
  'co_cbo',
  'microarea',
  'data',
  'co_ibge_mun',
 ];

//  php artisan make:migration create_profissionais_xml_esus_table --path=app/packages/JonatasCraveiro/importacao-xml-esus/src/database/migrations --create=profissionais_xml_esus

}
