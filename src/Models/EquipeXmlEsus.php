<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EquipeXmlEsus extends Model {
 protected $table    = 'equipe_xml_esus';
 protected $fillable = [

  'id_tp_equipe',
  'tp_equipe',
  'sg_equipe',
  'ds_equipe',
  'co_ine',
  'co_area',
  'ds_area',
  'nm_referencia',
  'dt_desativacao',
  'cnes_unidade',
  'ine_nasf',
  'data',
  'co_ibge_mun',
 ];

//  php artisan make:migration create_profissionais_xml_esus_table --path=app/packages/JonatasCraveiro/importacao-xml-esus/src/database/migrations --create=profissionais_xml_esus


}
