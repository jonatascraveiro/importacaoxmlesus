<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Exports;

use JonatasCraveiro\ImportacaoXmlEsus\Models\ProfissionalXmlEsus;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProfisionalXmlEsusExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return ProfissionalXmlEsus::select($this->headings())->get();
    }

    public function headings(): array
    {
        return [
           
            'co_ibge_mun',
            'data',
            'nm_prof',
            'cpf_prof',
            'co_cns',
            'dt_nasc',
            'sexo',
            'conselho_id',
            'sg_uf_emis',
            'nu_registro',
            'cnes',
            'co_ine',
            'co_cbo',
            'microarea',
           
        ];
    }
}
