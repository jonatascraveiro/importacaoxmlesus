<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Exports;

use JonatasCraveiro\ImportacaoXmlEsus\Models\EquipeXmlEsus;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EquipeXmlEsusExport implements FromCollection,WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return EquipeXmlEsus::select($this->headings())->get();
    }

    public function headings(): array
    {
        return [
            
            'co_ibge_mun',
            'data',
            'id_tp_equipe',
            'tp_equipe',
            'sg_equipe',
            'ds_equipe',
            'co_ine',
            'co_area',
            'ds_area',
            'nm_referencia',
            'dt_desativacao',
            'cnes_unidade',
          
           
        ];
    }
}
