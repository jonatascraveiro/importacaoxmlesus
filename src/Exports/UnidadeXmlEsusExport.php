<?php

namespace JonatasCraveiro\ImportacaoXmlEsus\Exports;

use JonatasCraveiro\ImportacaoXmlEsus\Models\UnidadeXmlEsus;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UnidadeXmlEsusExport implements FromCollection,WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return UnidadeXmlEsus::select($this->headings())->get();
    }

    public function headings(): array
    {
        return [
            'co_ibge_mun',
            'data',
            'nm_fanta',
            'cnpj',
            'cnes',
            'tp_unid_id',
            'co_esf_adm',
            'telefone1',
            'telefone2',
            'fax',
            'e_mail',
            'co_cep',
            'sg_uf',
            'bairro',
            'logradouro',
            'numero',
            'complement',
            'ponto_ref',
            'complexidade',
           
            
           
        ];
    }
}
