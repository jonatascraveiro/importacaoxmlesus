

## Importacao-xml-esus

    Pacote criado para importar as informacões de lotação de profissionais, equipes e unidades de saúde utilizada no sistema Esus-AB Pec

## Profissionais
    Possível verificar informações como ('nome profissional', 'cns', 'cpf', 'cnes' ,'ine')...;

## Equipe
    Possível verificar informações como ('nome da equipe', 'cnes', 'ine', 'tipologia da equipe'...;

## Unidade
    Possível verificar informações como ('nome da unidade', 'cnes', 'endereço', 'tipos de modalide'...;

# Instalação

    Instalando o pacote via composer:

    composer require jonatascraveiro/importacaoxmlesus

## Uso

    Após a instalação será adicionado ao projeto as migrations para adicionar as seguintes tabelas;
        * profissional_xml_esus *
        * unidade_xml_esus *
        * equipe_xml_esus *


## erro ao carregar xml

<li>Falha no Upload do arquivo xml.</li>

É necessário aumentar 'upload_max_filesize' padrão de 2M para 20M e 'post_max_size' padrão de 8M para 20M no arquivo php.ini
Após as alterações reiniciar a aplicação;

